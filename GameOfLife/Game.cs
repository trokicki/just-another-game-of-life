﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife {
	class Game {
		private List<Cell> cellsToDie;
		private List<Cell> cellsToBeReborn;
		public Cell[,] Cells { get; set; }
		public Size CellSize { get; private set; }
		public int[] CellPaddings { get; private set; }
		public int Steps { get; private set; }
		public int DeadCells {
			get {
				int deadCells = 0;
				foreach (var cell in Cells) {
					if (!cell.IsAlive) deadCells++;
				}
				return deadCells;
			}
		}
		public int AliveCells {
			get {
				int aliveCells = 0;
				foreach (var cell in Cells) {
					if (cell.IsAlive) aliveCells++;
				}
				return aliveCells;
			}
		}

		public Game(int rows = 42, int columns = 42) {
			Cells = new Cell[rows, columns];
			CellSize = new Size(14, 14);
			CellPaddings = new int[] { 1, 1 };
		}

		public void Step() {
			cellsToDie = new List<Cell>();
			cellsToBeReborn = new List<Cell>();
			int rows = Cells.GetLength(0);
			int columns = Cells.GetLength(1);
			//TODO: unstable, occasionaly exceptions are thrown in below foreach's
			Parallel.For(0, rows * columns, (i) => {
				handleCellSituation(i / rows, i % columns);
			});
			#region sequential approach
			//for (int i = 0; i < rows; i++) {
			//	for (int j = 0; j < columns; j++) {
			//		handleCellSituation(i, j);
			//	}
			//}
			#endregion
			foreach (Cell cell in cellsToBeReborn) {
				cell.IsAlive = true;
			}
			foreach (Cell cell in cellsToDie) {
				cell.IsAlive = false;
			}
			Steps++;
		}

		private void handleCellSituation(int row, int column) {
			var aliveCells = new List<Cell>();
			foreach (var cell  in Cells) {
				if (cell.IsAlive)
					aliveCells.Add(cell);
			}
			int neighbours = countNeighbours(row, column);
			if (Cells[row, column].IsAlive) {
				if (neighbours < 2) {
					cellsToDie.Add(Cells[row, column]);
				} else if (neighbours > 3) {
					cellsToDie.Add(Cells[row, column]);
				}
			} else if (neighbours == 3) {
				cellsToBeReborn.Add(Cells[row, column]);
			}
		}

		private int countNeighbours(int row, int column) {
			int neighbours = 0;
			if (checkNeighbour(row - 1, column - 1)) neighbours++;
			if (checkNeighbour(row - 1, column)) neighbours++;
			if (checkNeighbour(row - 1, column + 1)) neighbours++;
			if (checkNeighbour(row, column - 1)) neighbours++;
			if (checkNeighbour(row, column + 1)) neighbours++;
			if (checkNeighbour(row + 1, column - 1)) neighbours++;
			if (checkNeighbour(row + 1, column)) neighbours++;
			if (checkNeighbour(row + 1, column + 1)) neighbours++;
			return neighbours;
		}

		private bool checkNeighbour(int row, int column) {
			if (row > 0 && row < Cells.GetLength(0) && column > 0 && column < Cells.GetLength(1)) {
				return Cells[row, column].IsAlive;
			}
			return false;
		}

		public void InitializeCellsArray() {
			for (int row = 0; row < Cells.GetLength(0); row++) {
				for (int column = 0; column < Cells.GetLength(1); column++) {
					Cells[row, column] = new Cell() {
						IsAlive = false,
						Location = new Point(column * CellSize.Width, row * CellSize.Height)
					};
				}
			}
		}

		public void GenerateRandomAliveCells(int cellsAmount) {
			Random random = new Random();
			int generatedCells = 0;
			int deadCells = DeadCells;
			do{
				int row = random.Next(Cells.GetLength(0));
				int column = random.Next(Cells.GetLength(1));
				if (!Cells[row, column].IsAlive) {
					Cells[row, column].IsAlive = true;
					generatedCells++;
				}
			}while (generatedCells < Math.Min(cellsAmount, deadCells));
		}
	}
}
