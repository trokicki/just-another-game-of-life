﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameOfLife {
	public partial class MainForm : Form {
		private bool toggleCellTypeOnClick = true;
		private bool working = false;
		private Game game = new Game();

		public MainForm() {
			InitializeComponent();
			initializeCellsArray();
		}

		private void initializeCellsArray() {
			game.InitializeCellsArray();
			refreshBoard();
		}

		private void refreshBoard() {
			drawCells();
			StepsAmountLabel.Text = String.Format("Step: {0}", game.Steps);
		}

		private void drawCells() {
			foreach (var cell in game.Cells) {
				drawCell(cell);
			}
		}

		private void drawCell(Cell cell) {
			using (SolidBrush highlightBrush = new SolidBrush(Color.GreenYellow))
			using (SolidBrush aliveBrush = new SolidBrush(Color.Yellow))
			using (SolidBrush deadBrush = new SolidBrush(Color.Gray))
			using (Graphics graphics = DrawingPanel.CreateGraphics()) {
				Rectangle cellRectangle = new Rectangle(
					cell.Location.X + game.CellPaddings[0],
					cell.Location.Y + game.CellPaddings[1],
					game.CellSize.Width - game.CellPaddings[0],
					game.CellSize.Height - game.CellPaddings[1]);
				if (cell.Highlight) {
					graphics.FillRectangle(highlightBrush, cellRectangle);
				}
				else if (cell.IsAlive) {
					graphics.FillRectangle(aliveBrush, cellRectangle);
				} else {
					graphics.FillRectangle(deadBrush, cellRectangle);
				}
			}
		}

		private async Task RunWithInterval() {
			while (working) {
				await Step();
				await Task.Delay(1000 * 60 / SpeedTrackBar.Value);
			}
		}

		private async Task RunNTimes(int steps) {
			for (int i = 0; i < steps && working; i++) {
				await Step();
			}
		}

		private async Task Step() {
			await Task.Run(() => {
				game.Step();
				refreshBoard();
			});
		}

		private void DrawingPanel_Click(object sender, EventArgs e) {
			if (toggleCellTypeOnClick) {
				Point point = DrawingPanel.PointToClient(Cursor.Position);
				Cell cell = getCellByLocation(point);
				if (cell != null)
					toggleCellState(cell);
				drawCells();
			}
		}

		private Cell getCellByLocation(Point location) {
			int column = location.X / game.CellSize.Width;
			int row = location.Y / game.CellSize.Height;
			if (column >= game.Cells.GetLength(0) || row >= game.Cells.GetLength(1))
				return null;
			return game.Cells[row, column];
		}

		private void toggleCellState(Cell cell) {
			cell.IsAlive = !cell.IsAlive;
		}

		private async void StartButton_Click(object sender, EventArgs e) {
			if (!working) {
				working = true;
				WorkingLabel.Text = "Game is on";
				Task task = RunWithInterval();
				StartButton.Text = "Pause";
				Debug.Print(String.Format("Started"));
				await task;
			} else {
				working = false;
				WorkingLabel.Text = "Paused";
				StartButton.Text = "Start";
			}
		}

		private void DrawingPanel_Paint(object sender, PaintEventArgs e) {
			drawCells();
		}

		private void ClearButton_Click(object sender, EventArgs e) {
			initializeCellsArray();
		}

		private void GenerateRandomCellsButton_Click(object sender, EventArgs e) {
			int cellsAmount = Convert.ToInt32(NewRandomCellsAmountNUD.Value);
			game.GenerateRandomAliveCells(cellsAmount);
			drawCells();
		}

		private async void Run1000StepsButton_Click(object sender, EventArgs e) {
			int steps = 1000;
			Stopwatch stopwatch = Stopwatch.StartNew();
			working = true;
			WorkingLabel.Text = "Running 1000 steps";
			Task task = RunNTimes(steps);
			StartButton.Text = "Pause";
			await task;
			working = false;
			string result = String.Format("Running {0} steps complete. Total time elapsed: {1}", steps, stopwatch.Elapsed);
			WorkingLabel.Text = result;
			File.WriteAllText(DateTime.Now.ToFileTime().ToString(), result);
		}
	}
}
