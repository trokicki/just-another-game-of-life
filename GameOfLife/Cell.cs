﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife {
	class Cell {
		internal bool IsAlive { get; set; }
		internal Point Location { get; set; }
		internal bool Highlight { get; set; }
	}
}
