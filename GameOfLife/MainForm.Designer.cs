﻿namespace GameOfLife {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.DrawingPanel = new System.Windows.Forms.Panel();
			this.StartButton = new System.Windows.Forms.Button();
			this.SpeedTrackBar = new System.Windows.Forms.TrackBar();
			this.label1 = new System.Windows.Forms.Label();
			this.ClearButton = new System.Windows.Forms.Button();
			this.StatusStrip = new System.Windows.Forms.StatusStrip();
			this.StepsAmountLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.WorkingLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.label2 = new System.Windows.Forms.Label();
			this.GenerateRandomCellsButton = new System.Windows.Forms.Button();
			this.NewRandomCellsAmountNUD = new System.Windows.Forms.NumericUpDown();
			this.Run1000StepsButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.SpeedTrackBar)).BeginInit();
			this.StatusStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.NewRandomCellsAmountNUD)).BeginInit();
			this.SuspendLayout();
			// 
			// DrawingPanel
			// 
			this.DrawingPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.DrawingPanel.Location = new System.Drawing.Point(287, 0);
			this.DrawingPanel.Name = "DrawingPanel";
			this.DrawingPanel.Size = new System.Drawing.Size(597, 592);
			this.DrawingPanel.TabIndex = 0;
			this.DrawingPanel.Click += new System.EventHandler(this.DrawingPanel_Click);
			this.DrawingPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.DrawingPanel_Paint);
			// 
			// StartButton
			// 
			this.StartButton.Location = new System.Drawing.Point(16, 521);
			this.StartButton.Name = "StartButton";
			this.StartButton.Size = new System.Drawing.Size(261, 56);
			this.StartButton.TabIndex = 1;
			this.StartButton.Text = "Start";
			this.StartButton.UseVisualStyleBackColor = true;
			this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
			// 
			// SpeedTrackBar
			// 
			this.SpeedTrackBar.LargeChange = 10;
			this.SpeedTrackBar.Location = new System.Drawing.Point(12, 30);
			this.SpeedTrackBar.Maximum = 600;
			this.SpeedTrackBar.Minimum = 10;
			this.SpeedTrackBar.Name = "SpeedTrackBar";
			this.SpeedTrackBar.Size = new System.Drawing.Size(269, 45);
			this.SpeedTrackBar.SmallChange = 5;
			this.SpeedTrackBar.TabIndex = 2;
			this.SpeedTrackBar.TickFrequency = 10;
			this.SpeedTrackBar.Value = 120;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(124, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Speed (steps per minute)";
			// 
			// ClearButton
			// 
			this.ClearButton.Location = new System.Drawing.Point(16, 460);
			this.ClearButton.Name = "ClearButton";
			this.ClearButton.Size = new System.Drawing.Size(65, 23);
			this.ClearButton.TabIndex = 4;
			this.ClearButton.Text = "Clear";
			this.ClearButton.UseVisualStyleBackColor = true;
			this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
			// 
			// StatusStrip
			// 
			this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StepsAmountLabel,
            this.WorkingLabel});
			this.StatusStrip.Location = new System.Drawing.Point(0, 592);
			this.StatusStrip.Name = "StatusStrip";
			this.StatusStrip.Size = new System.Drawing.Size(884, 22);
			this.StatusStrip.TabIndex = 6;
			this.StatusStrip.Text = "statusStrip1";
			// 
			// StepsAmountLabel
			// 
			this.StepsAmountLabel.Name = "StepsAmountLabel";
			this.StepsAmountLabel.Size = new System.Drawing.Size(0, 17);
			// 
			// WorkingLabel
			// 
			this.WorkingLabel.Name = "WorkingLabel";
			this.WorkingLabel.Size = new System.Drawing.Size(0, 17);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 496);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(141, 13);
			this.label2.TabIndex = 8;
			this.label2.Text = "Generate random alive cells:";
			// 
			// GenerateRandomCellsButton
			// 
			this.GenerateRandomCellsButton.Location = new System.Drawing.Point(202, 492);
			this.GenerateRandomCellsButton.Name = "GenerateRandomCellsButton";
			this.GenerateRandomCellsButton.Size = new System.Drawing.Size(75, 23);
			this.GenerateRandomCellsButton.TabIndex = 10;
			this.GenerateRandomCellsButton.Text = "Generate";
			this.GenerateRandomCellsButton.UseVisualStyleBackColor = true;
			this.GenerateRandomCellsButton.Click += new System.EventHandler(this.GenerateRandomCellsButton_Click);
			// 
			// NewRandomCellsAmountNUD
			// 
			this.NewRandomCellsAmountNUD.Location = new System.Drawing.Point(154, 493);
			this.NewRandomCellsAmountNUD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.NewRandomCellsAmountNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.NewRandomCellsAmountNUD.Name = "NewRandomCellsAmountNUD";
			this.NewRandomCellsAmountNUD.Size = new System.Drawing.Size(42, 20);
			this.NewRandomCellsAmountNUD.TabIndex = 11;
			this.NewRandomCellsAmountNUD.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
			// 
			// Run1000StepsButton
			// 
			this.Run1000StepsButton.Location = new System.Drawing.Point(46, 262);
			this.Run1000StepsButton.Name = "Run1000StepsButton";
			this.Run1000StepsButton.Size = new System.Drawing.Size(203, 50);
			this.Run1000StepsButton.TabIndex = 12;
			this.Run1000StepsButton.Text = "Run 1000 steps";
			this.Run1000StepsButton.UseVisualStyleBackColor = true;
			this.Run1000StepsButton.Click += new System.EventHandler(this.Run1000StepsButton_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(884, 614);
			this.Controls.Add(this.Run1000StepsButton);
			this.Controls.Add(this.NewRandomCellsAmountNUD);
			this.Controls.Add(this.GenerateRandomCellsButton);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.StatusStrip);
			this.Controls.Add(this.ClearButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.SpeedTrackBar);
			this.Controls.Add(this.StartButton);
			this.Controls.Add(this.DrawingPanel);
			this.Name = "MainForm";
			this.Text = "Just another Game of Life";
			((System.ComponentModel.ISupportInitialize)(this.SpeedTrackBar)).EndInit();
			this.StatusStrip.ResumeLayout(false);
			this.StatusStrip.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.NewRandomCellsAmountNUD)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel DrawingPanel;
		private System.Windows.Forms.Button StartButton;
		private System.Windows.Forms.TrackBar SpeedTrackBar;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button ClearButton;
		private System.Windows.Forms.StatusStrip StatusStrip;
		private System.Windows.Forms.ToolStripStatusLabel StepsAmountLabel;
		private System.Windows.Forms.ToolStripStatusLabel WorkingLabel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button GenerateRandomCellsButton;
		private System.Windows.Forms.NumericUpDown NewRandomCellsAmountNUD;
		private System.Windows.Forms.Button Run1000StepsButton;
	}
}

